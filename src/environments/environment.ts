// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endPoints: {
    todos: 'https://jsonplaceholder.typicode.com/'
  },
  firebaseConfig: {
    apiKey: "AIzaSyDEtPgxjxtakfDEQ487HMN2TbdIqrOdtrU",
    authDomain: "pruebatodo-cc181.firebaseapp.com",
    databaseURL: "https://pruebatodo-cc181.firebaseio.com",
    projectId: "pruebatodo-cc181",
    storageBucket: "pruebatodo-cc181.appspot.com",
    messagingSenderId: "278532082404",
    appId: "1:278532082404:web:1eadf602722668df"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
