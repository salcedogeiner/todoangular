export const TODO_CONSTANTS = {
  EDIT: 'edit',
  CREATE: 'create',
  DELETE: 'delete',
  LIST: 'list',
  LISTDB: 'listdb',
};

export const SERVICES = {
  TODOS: 'todos',
};
