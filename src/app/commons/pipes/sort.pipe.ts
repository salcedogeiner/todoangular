import { Pipe, PipeTransform } from '@angular/core';

@Pipe({  name: 'sortBy' })
export class SortPipe implements PipeTransform {
  transform(array: Array < any > , args: string, type?: string ): Array < any > {
    if (array) {
      array.sort((a: any, b: any) => {
        if (type === 'number') {
          a[args] = +a[args];
          b[args] = +b[args];
        }
        if (a[args] < b[args]) {
          return -1;
        } else if (a[args] > b[args]) {
          return 1;
        } else {
          return 0;
        }
      });
    }
    return array;
  }
}
