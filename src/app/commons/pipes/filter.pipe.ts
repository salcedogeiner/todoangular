import { Pipe, PipeTransform } from '@angular/core';

@Pipe({  name: 'filterBy' })
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: any, args?: string): any[] {
    if (!searchText) {
      return items;
    }
    if (!items) {
      return [];
    }
    if (typeof searchText === 'object') {
      return [searchText];
    }
    if (!searchText || searchText === undefined) {
      return items;
    }
    const text = searchText.toLowerCase();
    if (args) {
      return items.filter(option => option[args].toLowerCase().includes(text));
    }

    return items.filter(option => {
      if (typeof(option) === 'object') {
        for (const property in option) {
          if (option[property] === null) {
            continue;
          }
          if (option[property].toString().toLowerCase().includes( text )) {
            return true;
          }
        }
      } else {
        return option.toLowerCase().includes(text);
      }
    });
  }
}
