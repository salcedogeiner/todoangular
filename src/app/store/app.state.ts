import { Todo } from '../models/todo';
export interface IAppState {
  listTodos: Todo[];
}
