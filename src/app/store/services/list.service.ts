
import { Injectable } from '@angular/core';
import { IAppState } from '../app.state';
import { Store } from '@ngrx/store';
import { REDUCER_LIST } from '../reducer.constants';
import { TodosService } from '../../services/todos.service';
import { SERVICES } from '../../commons/utils/constants';
@Injectable()
export class ListService {

  constructor(
    private todosService: TodosService,
    private store: Store < IAppState > ) {

  }

  /**
   * @method findTodos
   * @description
   * Service for save and list todos
   */
  public findTodos() {
    this.store.select(REDUCER_LIST.listTodos).subscribe(
      (list: any) => {
        if (!list || list.length === 0) {
          this.todosService.get(SERVICES.TODOS)
          .subscribe(
            (result: any[]) => {
              this.addList(REDUCER_LIST.listTodos, result);
            },
            error => {
              this.addList(REDUCER_LIST.listTodos, []);
            },
          );
        }
      },
    );
  }

  /**
   *
   * @method addList
   * @param {string} type
   * @param {Array < any >} object
   * @description
   * store todos on reducers
   */
  private addList(type: string, object: Array < any > ) {
    this.store.dispatch({
      type,
      payload: object,
    });
  }
}
