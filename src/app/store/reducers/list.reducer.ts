import { REDUCER_LIST } from '../reducer.constants';

export class ListReducer {
  constructor() {
  }
  static listReducerTodos(state: Array<any> =  new Array(), action) {
    switch (action.type) {
      case REDUCER_LIST.listTodos:
        return [...state, ...action.payload];
      default:
        return state;
      }
  }
}
