import { IAppState } from './app.state';
import { ListReducer } from './reducers/list.reducer';
import { ActionReducerMap } from '@ngrx/store';

export const rootReducer: ActionReducerMap<IAppState> = {
  listTodos: ListReducer.listReducerTodos,
};

