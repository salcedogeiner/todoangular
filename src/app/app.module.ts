import {SortPipe} from './commons/pipes/sort.pipe';
import {FilterPipe} from './commons/pipes/filter.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoListComponent } from './pages/todo-list/todo-list.component';
import { TodosService } from './services/todos.service';
import { TodoCrudComponent } from './pages/todo-crud/todo-crud.component';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ListService } from './store/services/list.service';
import { StoreModule } from '@ngrx/store';
import { rootReducer } from './store/rootReducer';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment'


@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoCrudComponent,
    SortPipe,
    FilterPipe
  ],
  imports: [
    StoreModule.forRoot(rootReducer),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,    
    AngularFirestoreModule,
  ],
  providers: [
    TodosService,
    ListService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
