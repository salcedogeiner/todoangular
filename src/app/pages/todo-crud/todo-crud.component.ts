import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TodosService } from '../../services/todos.service';
import { HttpErrorResponse } from '@angular/common/http';
import { TODO_CONSTANTS, SERVICES } from '../../commons/utils/constants';
import { AngularFirestore } from '@angular/fire/firestore';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-todo-crud',
  templateUrl: './todo-crud.component.html',
  styleUrls: ['./todo-crud.component.sass']
})
export class TodoCrudComponent implements OnInit {

  @Input() public mode: string; // mode variable for switch action form
  @Input() public todo: Todo; // todo selected variable

  public rsFormGroup: FormGroup; // form
  public message: string; // message resulting from the action

  constructor(
    private todosService: TodosService,
    private db: AngularFirestore,
  ) { }

  ngOnInit() {  

    this.rsFormGroup = new FormGroup({
      _id: new FormControl(''),
      id: new FormControl(''),
      title: new FormControl(''),
    });
    if (this.mode != TODO_CONSTANTS.CREATE) {
      this.rsFormGroup.setValue(this.todo);
    }
  }

  /**
   * @method submit
   * @description
   * execute action according mode variable
   *
   */
  submit() {
    switch (this.mode) {
      case TODO_CONSTANTS.CREATE:
        this.db.collection(SERVICES.TODOS).add(this.rsFormGroup.value).then(res => {
          console.log(res);          
        }).catch(error => {
          console.log(error);          
        });
        break;
      case TODO_CONSTANTS.EDIT:
          this.db.collection(SERVICES.TODOS).doc(this.rsFormGroup.value._id).set(this.rsFormGroup.value).then(res => {
            console.log(res);          
          }).catch(error => {
            console.log(error);          
          });
        break;
      case TODO_CONSTANTS.DELETE:
          this.db.collection(SERVICES.TODOS).doc(this.rsFormGroup.value._id).delete().then(res => {
            console.log(res);          
          }).catch(error => {
            console.log(error);          
          });
        break;
      default:
        break;
    }
  }

}
