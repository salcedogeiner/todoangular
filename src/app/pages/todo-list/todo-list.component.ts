import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../../models/todo';
import { HttpErrorResponse } from '@angular/common/http';
import { ListService } from '../../store/services/list.service';
import { Store, select } from '@ngrx/store';
import { IAppState } from '../../store/app.state';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {

  @Input() public mode: string; // mode variable for switch action form

  public todoList: Todo[]; // todo list
  public filterTitle: string; // filter variable
  @Output() public todoSelected = new EventEmitter<Todo>();

  constructor(
    private listService: ListService,
    private store: Store < IAppState >,
    private db: AngularFirestore,
  ) { }

  ngOnInit() {

    switch (this.mode) {
      case 'db':
          this.db.collection('todos').snapshotChanges().subscribe((res) => {
            console.log(res);
            this.todoList = [];
            res.forEach((todo: any) => {
              this.todoList.push({
                _id: todo.payload.doc.id,
                ...todo.payload.doc.data()
              });
            });
          });
        break;
      case 'service':
          this.listService.findTodos();
          this.loadLists();
          break;
    
      default:
        break;
    }
  }

  select ( todo: Todo ) {
    this.todoSelected.emit(todo);
  }

  /**
   * @method loadLists
   * @description
   * load todos from reducers
   */
  public loadLists() {
    this.store.select((state) => state).subscribe(
      ({listTodos}) => {
        this.todoList = listTodos;
      },
    );
  }

}
