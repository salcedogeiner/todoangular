import { Component } from '@angular/core';
import { NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import { TODO_CONSTANTS } from './commons/utils/constants';
import { TodoListComponent } from './pages/todo-list/todo-list.component';
import { Todo } from './models/todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  providers: [NgbTabsetConfig]
})
export class AppComponent {

  public todoSel: Todo;
  public flagSelect = true;

  // stock options
  modes = {
    edit: TODO_CONSTANTS.EDIT,
    create: TODO_CONSTANTS.CREATE,
    delete: TODO_CONSTANTS.DELETE,
    list: TODO_CONSTANTS.LIST,
    listdb: TODO_CONSTANTS.LISTDB,
    db: 'db',
    service: 'service',
  };

  title = 'TodoReactAngular';
  constructor(config: NgbTabsetConfig) {
    // customize default values of tabsets used by this component tree
    config.justify = 'fill';
    config.type = 'pills';
  }

  obtainTodo($event) {
    this.todoSel = $event;
    this.flagSelect = false;
  }
}
